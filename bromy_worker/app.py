import time
import json
import pika
from typing import Callable

from pika.channel import Channel as PikaChannel 
from pika.spec import Basic, BasicProperties as PikaBasicProperties 

from common import ExecutionResult, ExecutionScriptError, ExecutionStatus as STATUS
from config import Config
from schema import load_execution_trigger_payload
from logger import WLogger
from executor import execute

class WorkerApp():

  def __init__(self) -> None:
    try:
      WLogger.info(f'worker app initiating')
      connection_parameters = pika.ConnectionParameters(host=Config.RABBITMQ_HOST, virtual_host=Config.RABBITMQ_VIRTUAL_HOST, heartbeat=600)

      # Connect to the RabbitMQ broker
      self.connection = pika.BlockingConnection(connection_parameters)

      # Create a channel
      self.channel = self.connection.channel()

      self.channel.queue_declare(queue=Config.RABBITMQ_TRIGGER_QUEUE_NAME)
      self.channel.queue_declare(queue=Config.RABBITMQ_REPORT_QUEUE_NAME)

      self.channel.basic_consume(queue=Config.RABBITMQ_TRIGGER_QUEUE_NAME, on_message_callback=self.handle_exec_trigger_message)
      WLogger.info(f'worker app initiated')
    except Exception as err:
      WLogger.error(f'worker app setup error {err}')
      raise err


  def start_consuming(self):
    self.channel.start_consuming()

  def process_ack(self, ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, exec_result: ExecutionResult):
    ch.basic_publish(exchange='',
                     routing_key=Config.RABBITMQ_REPORT_QUEUE_NAME,
                     body=exec_result.dumps())
    json.loads(exec_result.dumps())
    ch.basic_ack(delivery_tag=method.delivery_tag)

  def process_exec_error(self, ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, err: Exception, execution_id: str, execution_time: int):
    WLogger.error(f'worker exec setup error {err}')
    exec_result = ExecutionResult(
      result='', 
      status=STATUS.ERROR, 
      log=WLogger.pop_log(), 
      execution_id=execution_id,
      execution_time=execution_time
    )
    self.process_ack(ch, method, properties, exec_result)

  def process_exec_script_failed(self, ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, err: ExecutionScriptError, execution_id: str, execution_time: int):
    WLogger.error(f'Worker exec script failed')
    exec_result = ExecutionResult(
      result=err.message,
      status=STATUS.FAILED, 
      log=WLogger.pop_log(), 
      execution_id=execution_id,
      execution_time=execution_time
    )
    self.process_ack(ch, method, properties, exec_result)

  def process_exec_script_success(self, ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, exec_output: str, execution_id: str, execution_time: int):
    WLogger.info(f'Worker exec script success')
    exec_result = ExecutionResult(
      result=exec_output,
      status=STATUS.SUCCESS, 
      log=WLogger.pop_log(), 
      execution_id=execution_id,
      execution_time=execution_time
    )
    self.process_ack(ch, method, properties, exec_result)

  def handle_exec_trigger_message(self, ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, body: bytes):
    WLogger.info(f'consuming trigger message {body}')
    exec_payload, err = load_execution_trigger_payload(body)
    if err:
      self.process_exec_error(ch, method, properties, err)
      return

    execution_id = exec_payload['execution_id']

    start_time = time.monotonic()
    try:
      exec_output = self.on_trigger_callback(**exec_payload)
      execution_time = int(time.monotonic() - start_time)
      self.process_exec_script_success(ch, method, properties, exec_output, execution_id, execution_time)
    except ExecutionScriptError as err:
      execution_time = int(time.monotonic() - start_time)
      self.process_exec_script_failed(ch, method, properties, err, execution_id, execution_time)
    except Exception as err:
      execution_time = int(time.monotonic() - start_time)
      self.process_exec_error(ch, method, properties, err, execution_id, execution_time)

  def bind_on_trigger_call_back(self, callback: Callable):
    self.on_trigger_callback = callback


app: WorkerApp = None

def init():
  global app
  app = WorkerApp()
  app.bind_on_trigger_call_back(execute)