import signal
import sys
import app
from logger import WLogger


def handle_sigterm(sig: int, frame: object):
  WLogger.info("Received SIGTERM, shutting down gracefully...")
  app.app.channel.close()
  sys.exit(0)


signal.signal(signal.SIGTERM, handle_sigterm)


if __name__ == "__main__":
    WLogger.bind_file_stream_log('worker-setup.log')
    app.init()    
    WLogger.reload_logger()
    try: 
        app.app.start_consuming()
    except KeyboardInterrupt:
        handle_sigterm(signal.SIGTERM, None)
