import sys
import json
import traceback
import textwrap
from script import runTest

# Check if there's at least one argument (script name + JSON string)
if len(sys.argv) < 2:
  sys.stderr.write("Error: Missing JSON string argument")
  sys.exit(1)

# Get the JSON string from the first argument (assuming it's the JSON data)
json_string = sys.argv[1]
max_character_count = 30000
if len(sys.argv) >= 3:
  max_character_count = int(sys.argv[2])

# Try loading the JSON string
try:
  data = json.loads(json_string)
  return_data = ''
  if bool(data):
    # NOTE: if the number of parameter accepted is different it will error
    return_data = runTest(**data)
  else:
    return_data = runTest()
  result = str(return_data)
  sys.stdout.write("SUCCESS\n")
  sys.stdout.write("Result size:" + str(len(result)) + "\n")
  sys.stdout.write("Script result:\n")
  sys.stdout.write(textwrap.shorten(result, width=max_character_count, placeholder='...'))
except json.JSONDecodeError as e:
  sys.stderr.write("Error: Invalid JSON format")
  sys.stderr.write(e.msg)
  sys.exit(1)
# except Exception as e:
#   ex_type, ex_value, ex_traceback = sys.exc_info()

#   # Extract unformatter stack traces as tuples
#   trace_back = traceback.extract_tb(ex_traceback)

#   # Format stacktrace
#   stack_trace = list()

#   for trace in trace_back:
#       stack_trace.append("File : %s , Line : %d, Func.Name : %s, Message : %s" % (trace[0], trace[1], trace[2], trace[3]))

#   sys.stderr.write("Error: Error in running scripts ")
#   sys.stderr.write(str(e) + '\n')
#   sys.stderr.write("Exception type : %s\n" % ex_type.__name__)
#   sys.stderr.write("Exception message : %s\n" %ex_value)
#   sys.stderr.write("Stack trace : %s\n" % stack_trace)
#   sys.exit(1)
