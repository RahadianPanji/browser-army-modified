import io
import logging
from importlib import reload

LOGGER_CONFIG = {
    'format': '%(asctime)s %(module)s %(levelname)s: %(message)s',
    'datefmt': '%m/%d/%Y %I:%M:%S %p',
    'level': logging.INFO
}

class WLogger:

    captured_stream: io.StringIO = None
    logger: logging.Logger = None

    @classmethod
    def bind_string_stream_log(cls) -> None:
        cls.captured_stream = io.StringIO()
        stream_handler = logging.StreamHandler(cls.captured_stream)
        cls.logger.addHandler(stream_handler)

    @classmethod
    def bind_file_stream_log(cls, file_name: str) -> None:
        file_handler = logging.FileHandler(file_name)
        cls.logger.addHandler(file_handler)

    @classmethod
    def reload_logger(cls, file_name: str = None) -> None:
        if not cls.logger:
            cls.logger = logging.getLogger()
            logging.basicConfig(**LOGGER_CONFIG)
        else:
            logging.shutdown()
            reload(logging)
            logging.basicConfig(**LOGGER_CONFIG)

        cls.bind_string_stream_log()
        if file_name != None:
            cls.bind_file_stream_log(file_name)
        
        cls.error = cls.logger.error
        cls.info = cls.logger.info
        cls.debug = cls.logger.debug
        cls.warning = cls.logger.warning

    error = None
    info = None
    debug = None
    warning = None

    @classmethod
    def pop_log(cls) -> str:
        # TODO: check why log is empty
        result_log = cls.captured_stream.getvalue()
        cls.reload_logger()
        return result_log


WLogger.reload_logger()
