from dataclasses import dataclass
from enum import Enum, auto
import json

class ExecutionStatus(Enum):
  SUCCESS = auto()
  FAILED = auto()
  ERROR = auto()

  def __str__(self):
    return self.name

@dataclass
class ExecutionResult:
  result: str
  status: ExecutionStatus
  log: str
  execution_id: str
  execution_time: int

  def dumps(self):
    result_dict = {}
    for key, value in self.__dict__.items():
      result_dict[key] = str(value)
    return json.dumps(result_dict)


class ExecutionScriptError(Exception):
  """Execution script return non-zero value."""

  def __init__(self, stderr):
    self.message = stderr