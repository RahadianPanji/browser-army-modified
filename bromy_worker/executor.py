import json
import subprocess
from typing import Dict, Tuple

from config import Config
from common import ExecutionScriptError
from logger import WLogger

# save script in the appropriate location
def save_script(script: str) -> int:
    for attempt in range(Config.EXECUTOR_SAVE_SCRIPT_TRIES):
        try:
            with open(Config.EXECUTOR_SCRIPT_PATH, 'w+') as file_stream:
                file_stream.write(script)
            return 0
        except Exception as e:
            WLogger.error(f'{__name__}: error in saving the script to path {Config.EXECUTOR_SCRIPT_PATH}, attempt {attempt+1}')
            WLogger.error(e)
    return 1

def execute_script(json_params: Dict[str, str]) -> Tuple[int, str]:
    process = subprocess.Popen([Config.EXECUTOR_PYTHON_ALIAS, Config.EXECUTOR_SCRIPT_MAIN_PATH, json.dumps(json_params), str(Config.SCRIPT_MAX_RETURN_SIZE)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    try:
        process.wait(Config.EXECUTOR_REAL_TIME_LIMIT) # waiting for CPU time need the help of cgroup
    except subprocess.CalledProcessError as e:
        WLogger.error(f'{__name__}: script runtime error')
        return process.returncode, e.stderr.decode()
    except subprocess.TimeoutExpired as e:
        WLogger.error(e.stdout)
        WLogger.error(e.stderr)
        WLogger.error(e.output)
        WLogger.error(e.cmd)
        return -1, "Script execution time limit exceeded"

    stdout, stderr = process.communicate()

    if process.returncode != 0:
        WLogger.error(f'{__name__}: script runtime error')
        return process.returncode, stderr

    return process.returncode, stdout

# executing script, returning:
# return_code, stdout -> if success (return code 0)  
# return_code, stderr -> if not error (return code != 0)  
def execute(script: str, send_keys_params: Dict[str, str], execution_id: str) -> str:
    if save_script(script):
        raise Exception("Saving script failed")
    
    return_code, output = execute_script(send_keys_params)
    if return_code != 0:
        raise ExecutionScriptError(output)
    return output
