import json
from typing import Tuple
from jsonschema import validate

# Define the JSON schema
EXECUTION_TRIGGER_SCHEMA = {
  "type": "object",
  "properties": {
    "script": {"type": "string"},
    "send_keys_params": {"type": "object"},
    "execution_id": {"type": "string"},
  },
  "required": ["script", "execution_id", "send_keys_params"],
}


def load_execution_trigger_payload(payload: bytes) -> Tuple[object, Exception]:
  data = json.loads(payload)

  # Validate the payload against the schema
  try:
    validate(data, EXECUTION_TRIGGER_SCHEMA)
    return data, None
  except json.JSONDecodeError as e:
    return None, Exception(f"Error parsing JSON: {e}")
  except Exception as e:
    return None, Exception(f"Validation error: {e}")
