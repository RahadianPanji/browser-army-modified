import logging

LOGGER_CONFIG = {
    'format': '[%(asctime)s] [%(levelname)s] [%(name)s.py:%(lineno)d] - %(message)s',
    'datefmt': '%Y-%m-%d %H:%M:%S',
    'level': logging.INFO
}
logging.basicConfig(**LOGGER_CONFIG)

def get_logger(name):
    return logging.getLogger(name)
