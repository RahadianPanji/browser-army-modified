from pika.channel import Channel as PikaChannel 
from pika.spec import Basic, BasicProperties as PikaBasicProperties 

from bromy_master.schemas import load_execution_report_payload
from bromy_execution import crud, service
from database import get_db_context
from logger import get_logger


logger = get_logger(__name__)

def handle_exec_report_message(ch: PikaChannel, method: Basic.Deliver, properties: PikaBasicProperties, body: bytes):
    logger.info(f'Consuming report message...')
    report, err = load_execution_report_payload(body)
    if err:
        logger.error(f'invalid report message format: {body}, got exception: {err}')
        return

    execution_id = report['execution_id']
    logger.info(f'Updating execution {execution_id} with its report...')

    with get_db_context() as session:
        err = crud.update_execution(db=session, update_dict=report)
        if err:
            logger.error(f'Error when updating execution {execution_id} with data {report}')
        else:
            logger.info(f'Updated execution {execution_id} with its report')

        db_exec_req = crud.get_parent_execution_request(db=session, execution_id=execution_id)
        exec_req_id = str(db_exec_req.id)
        
        logger.info("Checking if Execution Request status needs update...")
        service.check_and_update_execution_request_status(db=session, execution_request_id=exec_req_id)

    logger.info(f"Ack-ing message for execution {execution_id}...")
    ch.basic_ack(delivery_tag=method.delivery_tag)
    logger.info(f"Acked message for execution {execution_id}")
