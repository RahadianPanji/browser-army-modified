import json
from typing import Tuple
from jsonschema import validate

EXECUTION_REPORT_SCHEMA = {
    "type": "object",
    "properties": {
        "execution_id": {"type": "string"},
        "status": {"type": "string"},
        "result": {"type": "string"},
        "log": {"type": "string"},
        "execution_time": {"type": "integer"},
    },
    "required": ["execution_id", "status", "result", "log"],
}


def load_execution_report_payload(payload: bytes) -> Tuple[object, Exception]:
    # Validate the payload against the schema
    try:
        data = json.loads(payload)
        data["execution_time"] = int(data["execution_time"])
        validate(data, EXECUTION_REPORT_SCHEMA)
        return data, None
    except json.JSONDecodeError as e:
        return None, Exception(f"Error parsing JSON: {e}")
    except Exception as e:
        return None, Exception(f"Validation error: {e}")
