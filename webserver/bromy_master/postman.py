import pika
import requests
import json
import config
from concurrent.futures import ThreadPoolExecutor

from bromy_master.process_report import handle_exec_report_message
from bromy_master.util import partition_list
from bromy_execution import crud, models
from database import get_db_context
from logger import get_logger


logger = get_logger(__name__)
executor = ThreadPoolExecutor()

def init_connection(vhost_name: str):
    # Create vhost
    if not is_vhost_exist(vhost_name=vhost_name):
        create_vhost(vhost_name)

    # Connect to the RabbitMQ broker
    logger.info(f"Connecting to rabbitmq at {config.RABBITMQ_HOST} with vhost {vhost_name}...")
    connection_parameters = pika.ConnectionParameters(host=config.RABBITMQ_HOST, virtual_host=vhost_name)
    connection = pika.BlockingConnection(connection_parameters)

    # Create a channel
    channel = connection.channel()

    channel.queue_declare(queue=config.RABBITMQ_TRIGGER_QUEUE_NAME)
    channel.queue_declare(queue=config.RABBITMQ_REPORT_QUEUE_NAME)

    channel.basic_consume(queue=config.RABBITMQ_REPORT_QUEUE_NAME, on_message_callback=handle_exec_report_message)

    while True:
        try:
            channel.start_consuming()
        except Exception as e:
            logger.error(f"Got Exception when consuming report message: {e}")
            if not is_vhost_exist(vhost_name=vhost_name):
                break
            logger.info(f"Reconnecting to consume report message for vhost {vhost_name}...")
    
    logger.info(f"Stop consuming for vhost {vhost_name}...")
    if connection.is_open:
        connection.close()

def create_vhost(vhost_name: str):
    logger.info(f"Creating vhost {vhost_name}...")
    json_data = {
        'description': f'master worker connection for execution request id {vhost_name}',
        'tags': vhost_name,
    }
    response = requests.put(
        f'{config.RABBITMQ_MGMT_HOST}/api/vhosts/{vhost_name}',
        json=json_data,
        auth=(config.RABBITMQ_USERNAME, config.RABBITMQ_PASSWORD),
    )
    if response.ok:
        logger.info(f"Succeeded create vhost {vhost_name}")
    else:
        logger.error(f"Failed to create vhost {vhost_name}")
        return 1
    return 0

def resume_connection():
    logger.info("Resuming rabbitmq connection to ongoing execution requests...")

    # Fetch exec reqs id
    with get_db_context() as db_session:
        status_to_reconnect = [models.ExecutionRequestStatus.RUNNING, models.ExecutionRequestStatus.INFRA_CREATED]
        db_exec_reqs = crud.get_execution_request_by_status(db=db_session, statuses=status_to_reconnect)
        exec_reqs_id = [str(exec_req.id) for exec_req in db_exec_reqs]

    logger.info(f"Opening connection to rabbitmq for Execution Requests {exec_reqs_id}...")
    for exec_req_id in exec_reqs_id:
        executor.submit(init_connection, exec_req_id)
        logger.info(f"Created a thread to open rabbitmq connection for Execution Request {exec_req_id}")
        

def destroy_connection(exec_req_id: str):
    # Destroy vhost
    response = requests.delete(
        f'{config.RABBITMQ_MGMT_HOST}/api/vhosts/{exec_req_id}',
        auth=(config.RABBITMQ_USERNAME, config.RABBITMQ_PASSWORD),
    )
    if response.ok:
        logger.info(f"Succeeded destroy vhost {exec_req_id}")
    else:
        logger.error(f"Failed to destroy vhost {exec_req_id}")
        return 1
    return 0

def is_vhost_exist(vhost_name: str) -> bool:
    response = requests.get(
        f'{config.RABBITMQ_MGMT_HOST}/api/vhosts/{vhost_name}',
        auth=(config.RABBITMQ_USERNAME, config.RABBITMQ_PASSWORD),
    )
    if response.ok:
        return True
    elif response.status_code != 404:
        logger.error(f"Unexpected response code {response.status_code} "
                     "when checking existence of vhost {vhost_name}")
    return False

def trigger_worker(exec_req: models.BromyExecutionRequest):
    exec_req_id = str(exec_req.id)

    if not exec_req.infra_on_demand:
        vhosts = [f"{config.RABBITMQ_PERSISTENT_VHOST_PREFIX}-{zone}" for zone in exec_req.infra_zone]
    if exec_req.infra_on_demand:
        vhosts = [exec_req_id]

    # Distribute executions to each vhost
    exec_vhost_mapping = {}
    partitioned_execs = partition_list(exec_req.executions, len(vhosts))
    for i in range(len(vhosts)):
        exec_vhost_mapping[vhosts[i]] = partitioned_execs[i]

    for vhost in exec_vhost_mapping.keys():
        # Connect to the RabbitMQ broker
        logger.info(f"Connecting to rabbitmq at {config.RABBITMQ_HOST} vhost {vhost}")
        connection_parameters = pika.ConnectionParameters(host=config.RABBITMQ_HOST, virtual_host=vhost)
        connection = pika.BlockingConnection(connection_parameters)

        # Create channel
        channel = connection.channel()
        channel.queue_declare(queue=config.RABBITMQ_TRIGGER_QUEUE_NAME)

        # Publish trigger message
        logger.info(f"Publishing trigger message for exec_req_id={exec_req_id} vhost={vhost}...")
        for execution in exec_vhost_mapping[vhost]:
            msg = {
                "script": exec_req.selenium_script,
                "send_keys_params": json.loads(execution.send_keys_param),
                "execution_id": str(execution.id),
                "side_runner_mode": exec_req.side_runner_mode
            }
            channel.basic_publish(exchange='',
                        routing_key=config.RABBITMQ_TRIGGER_QUEUE_NAME,
                        body=json.dumps(msg))
        
        logger.info(f"Done publishing trigger message for exec_req_id={exec_req_id} vhost={vhost}. Closing connection...")
        connection.close()
        logger.info(f"Connection closed for trigger message for exec_req_id={exec_req_id} vhost={vhost}")

    return 0
