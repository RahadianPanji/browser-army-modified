def partition_list(lst: list, n: int) -> list[list]:
    """ 
    Partition a list into n lists 
    partition_list([1, 2, 3, 4, 5, 6, 7], 3)  ->  [ [1, 2, 3], [4, 5], [6, 7] ]
    """

    # Calculate the size of each partition and the remainder
    partition_size, leftover = divmod(len(lst), n)

    # Create the partitions
    ret = []
    left = 0
    for i in range(n):
        right = left + partition_size
        if leftover:
            right += 1
            leftover -= 1
        ret.append(lst[left:right])
        left = right
    return ret
