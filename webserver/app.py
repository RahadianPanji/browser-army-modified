from fastapi import FastAPI

from lifespan import lifespan
from database import engine
from bromy_execution import models
from bromy_execution.controller import bromy_router

models.Base.metadata.create_all(bind=engine)

app = FastAPI(lifespan=lifespan)

app.include_router(bromy_router)
