import ansible_runner
import os
import config

class AnsiblePlaybookRunner:
    playbook_path = os.path.dirname(os.path.realpath(__file__))

    def __init__(self, execution_request_id: str, 
                script: str, 
                zone: list[str] = ["us-central1-a"],
                machine_type: list[str] = ["e2-micro"],
                worker_count: list[int] = [1],
                gcp_project=config.GCP_PROJECT,
                region="us-central1", 
                cluster_name=config.CLUSTER_NAME,
                disk_size_gb=25):

        self.params = {
            'execution_request_id': execution_request_id,
            'script': script,
            'zone': zone,
            'machine_type': machine_type,
            'worker_count': worker_count,
            'gcp_project': gcp_project,
            'region': region,
            'cluster_name': cluster_name,
            'disk_size_gb': disk_size_gb,

            'service_account': config.GCP_SERVICE_ACCOUNT,
            'kube_config': config.KUBE_CONFIG,
            'RABBITMQ_HOST': config.RABBITMQ_HOST,

            'node_count': 1
        }

        with open("./bromy_execution/ansible_playbooks/kubeconfig", "w") as f:
            f.write(config.KUBE_CONFIG)

    @staticmethod
    def ensure_persistent_nodepool():
        params = {
            "gcp_project": config.GCP_PROJECT,
            "region": "us-central1", 
            "cluster_name": config.CLUSTER_NAME,
            "machine_type": "e2-small",
            "disk_size_gb": 25,
            "service_account": config.GCP_SERVICE_ACCOUNT,
            "node_count": 1,
        }

        playbook_path = f"{AnsiblePlaybookRunner.playbook_path}/create-persistent-nodepool.yml"
        r = ansible_runner.run(playbook=playbook_path, envvars=params, verbosity=1)

        if r.status != 'successful':
            raise Exception("Playbook create-nodepool didn't run successfully")
        
        created_node_pool = r.get_fact_cache("localhost")['created_node_pool']
        return created_node_pool

    @staticmethod
    def ensure_persistent_worker(zones):
        params = {
            "kube_config": config.KUBE_CONFIG,
            "worker_count": 2,
            "RABBITMQ_HOST": config.RABBITMQ_HOST,
        }
        
        # refers to this https://stackoverflow.com/questions/77392152/return-integer-value-without-quote-when-using-variable
        os.environ["ANSIBLE_JINJA2_NATIVE"] = "true"
        for zone in zones:
            params["zone"] = zone
            params["RABBITMQ_VHOST"] = f"{config.RABBITMQ_PERSISTENT_VHOST_PREFIX}-{zone}"

            playbook_path = f"{AnsiblePlaybookRunner.playbook_path}/create-persistent-deployment.yml"
            r = ansible_runner.run(playbook=playbook_path, envvars=params, verbosity=1)
            
            if r.status != 'successful':
                raise Exception("Playbook create-deployment didn't run successfully")
        
        del os.environ['ANSIBLE_JINJA2_NATIVE']

        
    def run_create_nodepool(self):
        print(f"Creating node pool for execution request {self.params['execution_request_id']}")

        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/create-nodepool.yml", envvars=self.params, verbosity=1)

        if r.status != 'successful':
            raise Exception("Playbook create-nodepool didn't run successfully")
        
        created_node_pool = r.get_fact_cache("localhost")['created_node_pool']
        return created_node_pool

    def run_delete_nodepool(self):
        print(f"Deleting node pool for execution request {self.params['execution_request_id']}")

        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/delete-nodepool.yml", envvars=self.params, verbosity=1)
        
        if r.status != 'successful':
            raise Exception("Playbook delete-nodepool didn't run successfully")
        
        deleted_node_pool = r.get_fact_cache("localhost")['deleted_node_pool']
        return deleted_node_pool

    def run_create_deployment(self):
        print(f"Creating deployment for execution request {self.params['execution_request_id']}")

        # refers to this https://stackoverflow.com/questions/77392152/return-integer-value-without-quote-when-using-variable
        os.environ["ANSIBLE_JINJA2_NATIVE"] = "true"
        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/create-deployment.yml", envvars=self.params, verbosity=1)
        del os.environ['ANSIBLE_JINJA2_NATIVE']
        
        if r.status != 'successful':
            raise Exception("Playbook create-deployment didn't run successfully")
        
        created_deployment = r.get_fact_cache("localhost")['created_deployment']
        return created_deployment
        
    def run_delete_deployment(self):
        print(f"Deleting deployment for execution request {self.params['execution_request_id']}")

        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/delete-deployment.yml", envvars=self.params, verbosity=1)
        
        if r.status != 'successful':
            raise Exception("Playbook delete-deployment didn't run successfully")
        
        deleted_deployment = r.get_fact_cache("localhost")['deleted_deployment']
        return deleted_deployment
        
    def run_trigger_workers(self, master_pod_name, workers_ip):
        self.params['master_pod_name'] = master_pod_name
        self.params['workers_ip'] = workers_ip

        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/trigger-workers.yml", envvars=self.params, verbosity=1)
        
        if r.status != 'successful':
            raise Exception("Playbook trigger-workers didn't run successfully")
        
        return

    def run_get_worker_pods(self):
        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/get-worker-pods.yml", envvars=self.params, verbosity=1)

        if r.status != 'successful':
            raise Exception("Playbook get-worker-ip didn't run successfully")

        pods_info = r.get_fact_cache("localhost")['pods_info']
        return pods_info

    def run_get_master_pod(self):
        r = ansible_runner.run(playbook=f"{AnsiblePlaybookRunner.playbook_path}/get-master-pod.yml", envvars=self.params, verbosity=1)

        if r.status != 'successful':
            raise Exception("Playbook get-master-pod didn't run successfully")

        pods_info = r.get_fact_cache("localhost")['pods_info']
        return pods_info

    def get_master_pod_name(self):
        pods = self.run_get_master_pod()
        return pods[0]['metadata']['name']

    def get_worker_ip_address(self):
        pods = self.run_get_worker_pods()
        return [ pod['status']['podIP'] for pod in pods ]
