from fastapi import HTTPException
import json
from sqlalchemy import Column
from sqlalchemy.orm import Session

from bromy_execution import schemas
from bromy_execution.models import BromyExecution, BromyExecutionRequest, ExecutionStatus, ExecutionRequestStatus
from logger import get_logger


logger = get_logger(__name__)

def get_all_execution_request(db: Session, param: dict) -> list[BromyExecutionRequest]:
    db_query = db.query(BromyExecutionRequest)

    if param.get('id'):
        db_query = db_query.filter(BromyExecutionRequest.id == param.get('id'))
    if param.get('project_name'):
        db_query = db_query.filter(BromyExecutionRequest.project_name == param.get('project_name'))
    if param.get('test_name'):
        db_query = db_query.filter(BromyExecutionRequest.test_name == param.get('test_name'))
    if param.get('status'):
        db_query = db_query.filter(BromyExecutionRequest.status == param.get('status'))
    if param.get('infra_on_demand') != None:
        db_query = db_query.filter(BromyExecutionRequest.infra_on_demand == param.get('infra_on_demand'))

    return db_query.all()

def get_execution_request(db: Session, execution_request_id: str) -> BromyExecutionRequest:
    return db.query(BromyExecutionRequest).get(execution_request_id)

def get_execution_request_by_status(db: Session, statuses: list[ExecutionRequestStatus]) -> list[BromyExecutionRequest]:
    return db.query(BromyExecutionRequest).filter(
        BromyExecutionRequest.status.in_(statuses)
    ).all()

def get_parent_execution_request(db: Session, execution_id: str) -> BromyExecutionRequest:
    db_exec = db.query(BromyExecution).get(execution_id)
    
    if not db_exec:
        raise HTTPException(status_code=404, detail=f"Execution with id {execution_id} does not exist")

    return db_exec.execution_request

def get_all_execution(db: Session) -> list[BromyExecution]:
    return db.query(BromyExecution).all()

def get_execution(db: Session, execution_id: str) -> BromyExecution:
    return db.query(BromyExecution).get(execution_id)

def get_execution_by_status(db: Session, execution_request_id: str, statuses: list[ExecutionStatus]) -> list[BromyExecution]:
    return db.query(BromyExecution).filter(
        BromyExecution.execution_request_id == execution_request_id,
        BromyExecution.status.in_(statuses)
    ).all()

def create_execution_request(db: Session, create_schema: schemas.CreateExecutionRequest) -> BromyExecutionRequest:
    logger.info(f"Creating execution request with metadata {create_schema.projectMetadata}...")

    db_execs = []
    send_keys_param = create_schema.scriptParams.sendKeysParams.copy()
    exec_count = create_schema.scriptParams.execCount
    for i in range(0, exec_count):
        keys_param = {}
        for param_name, param_values in send_keys_param.items():
            if i >= len(param_values):
                keys_param[param_name] = param_values[-1]
            else:
                keys_param[param_name] = param_values[i]

        db_exec = BromyExecution(
            status = ExecutionStatus.NOT_STARTED,
            result = "Script has not been started yet.",
            send_keys_param = json.dumps(keys_param)
        )
        db_execs.append(db_exec)

    logger.info(f"Succeed generated Executions")

    infra_on_demand = create_schema.infraParams.useOnDemandInfra
    destroy_infra_after_finish = create_schema.infraParams.destroyInfraAfterFinish
    infra_zone = [param.zone for param in create_schema.infraParams.nodesConfig]
    infra_machine = [param.machineType for param in create_schema.infraParams.nodesConfig]
    infra_bot_count = [param.botCount for param in create_schema.infraParams.nodesConfig]

    db_exec_req = BromyExecutionRequest(
        project_name = create_schema.projectMetadata.projectName,
        test_name = create_schema.projectMetadata.testName,
        status = ExecutionRequestStatus.NOT_STARTED,
        selenium_script = create_schema.seleniumScript,
        infra_on_demand = infra_on_demand,
        destroy_infra_after_finish = destroy_infra_after_finish,
        infra_zone = infra_zone,
        infra_machine = infra_machine,
        infra_bot_count = infra_bot_count,
        script_exec_count = create_schema.scriptParams.execCount,
        side_runner_mode = create_schema.scriptParams.sideRunnerMode,
        script_send_keys_param = json.dumps(create_schema.scriptParams.sendKeysParams),
        executions = db_execs
    )

    logger.info(f"Succeed generated Execution Request. Committing to database...")

    db.add(db_exec_req)
    db.commit()

    logger.info(f"Committed created Execution Request to database")

    return db_exec_req

def delete_execution_request(db: Session, exec_req_id: str):
    db_exec_req = get_execution_request(db=db, execution_request_id=exec_req_id)
    
    if not db_exec_req:
        raise HTTPException(status_code=404, detail=f"Execution request {exec_req_id} does not exist")

    logger.info(f"Deleting execution request {exec_req_id}...")
    db.delete(db_exec_req)
    db.commit()
    logger.info(f"Deleted execution request {exec_req_id}")

    return 0

def update_execution_request_status(db: Session, exec_req_id: str, status: ExecutionRequestStatus) -> int:
    logger.info(f"Updating Execution Request {exec_req_id} status to {status}...")
    try:
        db_exec_req: BromyExecutionRequest = db.query(BromyExecutionRequest).get(exec_req_id)
        db_exec_req.status = status

        # if an ExecutionRequest status is set to RUNNING, set its Executions too
        if status == ExecutionRequestStatus.RUNNING:
            for execution in db_exec_req.executions:
                execution.status = ExecutionStatus.RUNNING
        db.commit()

    except Exception as e:
        logger.error(f"Got exception when updating Execution Request {exec_req_id} status to {status}: {e}")
        return 1
    return 0

def update_execution(db: Session, update_dict: dict) -> int:
    exec_id = update_dict.get('execution_id', None)
    if not exec_id:
        return 1
    
    logger.info(f"Updating Execution {exec_id}...")

    db_exec: BromyExecution = db.query(BromyExecution).get(exec_id)
    if not db_exec:
        return 1

    status = update_dict.get('status', None)
    if status:
        db_exec.status = status

    result = update_dict.get('result', None)
    if result:
        db_exec.result = result

    log = update_dict.get('log', None)
    if log:
        db_exec.log = log

    execution_time = update_dict.get('execution_time', None)
    if execution_time:
        db_exec.execution_time = execution_time

    db.commit()
    return 0


def update_execution_request_field(db: Session, exec_req_id: str, column: Column, field_value: int):
    logger.info(f"Updating Execution Request {exec_req_id} field {column} with {field_value}...")

    db_exec_req: BromyExecutionRequest = db.query(BromyExecutionRequest).get(exec_req_id)

    if not db_exec_req:
        return 1
    
    match column:
        case BromyExecutionRequest.infra_creation_time:
            db_exec_req.infra_creation_time = field_value

        case BromyExecutionRequest.infra_deletion_time:
            db_exec_req.infra_deletion_time = field_value
            
        case BromyExecutionRequest.script_execution_start:
            db_exec_req.script_execution_start = field_value
        case BromyExecutionRequest.script_execution_end:
            db_exec_req.script_execution_end = field_value

        case _:
            return 1

    db.commit()
    return 0
