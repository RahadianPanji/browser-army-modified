from fastapi import APIRouter, HTTPException, BackgroundTasks, Depends
import uuid

from bromy_execution import schemas, crud, service, models
from logger import get_logger
from database import get_db

bromy_router = APIRouter()
logger = get_logger(__name__)

@bromy_router.get("/execution/", response_model=list[schemas.ExecutionSchema])
def get_all_execution(db = Depends(get_db)):
    logger.info(f"Got request get_all_execution...")

    db_execs = crud.get_all_execution(db)

    response = []
    for db_exec in db_execs:
        response.append(schemas.ExecutionSchema.from_models(db_exec))

    return response

@bromy_router.get("/execution/{id}/", response_model=schemas.ExecutionSchema)
def get_execution(id: str, db = Depends(get_db)):
    logger.info(f"Got request get_execution with id {id}...")

    db_exec = crud.get_execution(db=db, execution_id=id)

    if not db_exec:
        raise HTTPException(status_code=404, detail=f"Execution with id {id} doesn't exist")

    return schemas.ExecutionSchema.from_models(db_exec)

@bromy_router.get("/execution-request/", response_model=list[schemas.ExecutionRequestSchema])
def get_all_execution_request(db = Depends(get_db), id: uuid.UUID = None, 
                              project_name: str = None, test_name: str = None, 
                              status: models.ExecutionRequestStatus = None, 
                              use_on_demand_infra: bool = None):
    
    logger.info(f"Got request get_all_execution_request...")

    param = {
        "id": id,
        "project_name": project_name,
        "test_name": test_name,
        "status": status,
        "infra_on_demand": use_on_demand_infra,
    }
    logger.info(f"Got request get_all_execution_request with parameter {param}")
    db_exec_reqs = crud.get_all_execution_request(db, param)

    response = []
    for db_exec_req in db_exec_reqs:
        response.append(schemas.ExecutionRequestSchema.from_models(db_exec_req))

    return response

@bromy_router.get("/execution-request/{id}/", response_model=schemas.ExecutionRequestSchema)
def get_execution_request(id: str, db = Depends(get_db)):
    logger.info(f"Got request get_execution_request with id {id}...")

    db_exec_req = crud.get_execution_request(db=db, execution_request_id=id)

    if not db_exec_req:
        raise HTTPException(status_code=404, detail=f"Execution Request with id {id} doesn't exist")

    return schemas.ExecutionRequestSchema.from_models(db_exec_req)

@bromy_router.post("/execution-request/create/", response_model=schemas.ExecutionRequestSchema)
def create_execution_request(body: schemas.CreateExecutionRequest, db = Depends(get_db)):
    logger.info(f"Got request create_execution_request with project metadata {body.projectMetadata}...")

    db_exec_req = crud.create_execution_request(db=db, create_schema=body)

    return schemas.ExecutionRequestSchema.from_models(db_exec_req)

@bromy_router.delete("/execution-request/", response_model=None, status_code=204)
def delete_execution_request(body: schemas.DeleteExecutionRequest, db = Depends(get_db)):
    logger.info(f"Got request delete_execution_request with body {body}...")

    err = crud.delete_execution_request(db=db, exec_req_id=body.id)
    if err:
        raise HTTPException(status_code=500, detail=f"")
    
    return None

@bromy_router.post("/execution-request/{id}/create-infra/", response_model=None, status_code=201)
def create_infra(id: str, background_tasks: BackgroundTasks, db = Depends(get_db)):
    logger.info(f"Got request create_infra for execution request id {id}...")

    return service.create_infra(db, id, background_tasks)

@bromy_router.post("/execution-request/{id}/destroy-infra/", response_model=None, status_code=204)
def destroy_infra(id: str, db = Depends(get_db)):
    logger.info(f"Got request destroy_infra for execution request id {id}...")

    return service.destroy_infra(db, id)

@bromy_router.post("/execution-request/{id}/run/", response_model=None, status_code=202)
def run_execution_request(id: str, background_tasks: BackgroundTasks, db = Depends(get_db)):
    logger.info(f"Got request run_execution_request for execution request id {id}...")

    return service.run_execution_request(db, id, background_tasks)
