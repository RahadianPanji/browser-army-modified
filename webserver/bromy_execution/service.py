import time
from fastapi import HTTPException, BackgroundTasks
from sqlalchemy.orm import Session
from concurrent.futures import ThreadPoolExecutor

from bromy_execution.ansible_playbooks.playbook_runner import AnsiblePlaybookRunner
from bromy_execution.models import BromyExecutionRequest, ExecutionRequestStatus, ExecutionStatus
from bromy_execution import crud
from bromy_master import postman
from logger import get_logger
import config

executor = ThreadPoolExecutor()
logger = get_logger(__name__)

def create_infra(db: Session, execution_request_id: str, background_tasks: BackgroundTasks):
    db_exec_req = crud.get_execution_request(db, execution_request_id)

    if not db_exec_req:
        raise HTTPException(status_code=404, detail=f"Execution Request with id {execution_request_id} doesn't exist")
    if not db_exec_req.infra_on_demand:
        raise HTTPException(status_code=400, detail=f"Execution Request {execution_request_id} infra is not on demand")

    start_time = time.monotonic()

    # Create rabbitmq vhost and listen for report
    postman.create_vhost(vhost_name=execution_request_id)
    background_tasks.add_task(postman.init_connection, vhost_name=execution_request_id)

    # Create infra (node pool and worker instance)
    runner = AnsiblePlaybookRunner(
        execution_request_id = execution_request_id, 
        script = db_exec_req.selenium_script,
        # TODO: implement multiple params (remove the [0])
        zone = db_exec_req.infra_zone[0],
        machine_type = db_exec_req.infra_machine[0],
        worker_count = db_exec_req.infra_bot_count[0],
    )

    try:
        runner.run_create_nodepool()
    except:
        raise HTTPException(status_code=500, detail="Ansible Playbook failed when creating nodepool")

    try:
        runner.run_create_deployment()
    except:
        raise HTTPException(status_code=500, detail="Ansible Playbook failed when creating deployment")
    
    # Update status to INFRA_CREATED
    err = crud.update_execution_request_status(db, exec_req_id=execution_request_id, status=ExecutionRequestStatus.INFRA_CREATED)
    if err:
        logger.error(f"Failed when updating Execution Request {execution_request_id} status to INFRA_CREATED")

    end_time = time.monotonic()
    create_infra_time = int(end_time - start_time)
    logger.info(f"Create infra for {execution_request_id} succeed in {create_infra_time} seconds")
    crud.update_execution_request_field(db, execution_request_id, BromyExecutionRequest.infra_creation_time, create_infra_time)


def destroy_infra(db: Session, execution_request_id: str):
    db_exec_req = crud.get_execution_request(db, execution_request_id)

    if not db_exec_req:
        raise HTTPException(status_code=404, detail=f"Execution Request with id {execution_request_id} doesn't exist")
    if not db_exec_req.infra_on_demand:
        raise HTTPException(status_code=400, detail=f"Execution Request {execution_request_id} infra is not on demand")

    start_time = time.monotonic()

    runner = AnsiblePlaybookRunner(
        execution_request_id = execution_request_id, 
        script = db_exec_req.selenium_script,
        # TODO: implement multiple params (remove the [0])
        zone = db_exec_req.infra_zone[0],
        machine_type = db_exec_req.infra_machine[0],
        worker_count = db_exec_req.infra_bot_count[0],
    )

    try:
        runner.run_delete_deployment()
    except:
        raise HTTPException(status_code=500, detail="Ansible Playbook failed when deleting deployment")

    try:
        runner.run_delete_nodepool()
    except:
        raise HTTPException(status_code=500, detail="Ansible Playbook failed when deleting nodepool")
    
    # Destroy rabbitmq vhost
    postman.destroy_connection(execution_request_id)

    end_time = time.monotonic()
    destroy_infra_time = int(end_time - start_time)
    logger.info(f"Destroy infra for {execution_request_id} succeed in {destroy_infra_time} seconds")
    crud.update_execution_request_field(db, execution_request_id, BromyExecutionRequest.infra_deletion_time, destroy_infra_time)


def run_execution_request(db: Session, execution_request_id: str, background_tasks: BackgroundTasks):
    db_exec_req = crud.get_execution_request(db, execution_request_id)

    if not db_exec_req:
        raise HTTPException(status_code=404, detail=f"Execution Request with id {execution_request_id} doesn't exist")
    if db_exec_req.status == ExecutionRequestStatus.RUNNING:
        raise HTTPException(status_code=400, detail=f"Execution Request with id {execution_request_id} is already running")

    create_infra_first = db_exec_req.infra_on_demand and db_exec_req.status != ExecutionRequestStatus.INFRA_CREATED
    if create_infra_first:
        create_infra(db, execution_request_id, background_tasks)

    start_time = int(time.time())

    err = postman.trigger_worker(db_exec_req)
    if err:
        raise HTTPException(status_code=500, detail=f"Failed publishing message to worker trigger queue")
    
    err = crud.update_execution_request_status(db, execution_request_id, ExecutionRequestStatus.RUNNING)
    if err:
        raise HTTPException(
            status_code=500, 
            detail=f"Got exception when updating Execution Request {execution_request_id} status to RUNNING")
    
    crud.update_execution_request_field(db, execution_request_id, BromyExecutionRequest.script_execution_start, start_time)
    

def check_and_update_execution_request_status(db: Session, execution_request_id: str):
    unfinished_executions = crud.get_execution_by_status(db, execution_request_id, 
                                                         statuses=[ExecutionStatus.NOT_STARTED, 
                                                                   ExecutionStatus.RUNNING])
    logger.info(f"Unfinished execution count: {len(unfinished_executions)}")

    if not unfinished_executions:
        logger.info(f"No unfinished executions. Updating Execution Request status...")

        db_exec_req = crud.get_execution_request(db, execution_request_id)

        success_exist = failed_exist = False
        for execution in db_exec_req.executions:
            if execution.status == ExecutionStatus.FAILED:
                failed_exist = True
            if execution.status == ExecutionStatus.SUCCESS:
                success_exist = True

        if success_exist and failed_exist:
            new_status = ExecutionRequestStatus.PARTIAL_SUCCESS
        elif success_exist:
            new_status = ExecutionRequestStatus.SUCCESS
        else:
            new_status = ExecutionRequestStatus.FAILED
    
        logger.info(f"Updating status for Execution Request {execution_request_id} to {new_status}...")
        err = crud.update_execution_request_status(db, execution_request_id, new_status)
        if err:
            logger.error(f"Failed to update status for Execution Request {execution_request_id} to {new_status}")
        else:
            logger.info(f"Updated status for Execution Request {execution_request_id} to {new_status}")

        logger.info(f"Updating script end time for {execution_request_id}...")
        end_time = int(time.time())
        err = crud.update_execution_request_field(db, execution_request_id, 
                                            BromyExecutionRequest.script_execution_end, end_time)
        if err:
            logger.error(f"Failed to update script end time for {execution_request_id}")
        else:
            logger.info(f"Updated script end time for {execution_request_id}")

        if db_exec_req.infra_on_demand:
            if db_exec_req.destroy_infra_after_finish:
                logger.info(f"Destroying infra for Execution Request {execution_request_id}...")
                destroy_infra(db, execution_request_id)
            else:
                logger.info(f"Not destroying infra for Execution Request {execution_request_id}")


def ensure_persistent_infra():
    zones = [
        "us-central1-a",
        "us-central1-b",
        "us-central1-c",
        "us-central1-f"
    ]
    vhosts = [
        f"{config.RABBITMQ_PERSISTENT_VHOST_PREFIX}-{zone}" for zone in zones
    ]
    for vhost in vhosts:
        postman.create_vhost(vhost)
        executor.submit(postman.init_connection, vhost)
    AnsiblePlaybookRunner.ensure_persistent_nodepool()
    AnsiblePlaybookRunner.ensure_persistent_worker(zones)
