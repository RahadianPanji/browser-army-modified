import uuid
from enum import Enum
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, BigInteger 
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import ENUM as pgEnum, ARRAY as pgArray, UUID as pgUUID
from database import Base

class ExecutionRequestStatus(str, Enum):
    SUCCESS = 'SUCCESS'
    PARTIAL_SUCCESS = 'PARTIAL_SUCCESS'
    FAILED = 'FAILED'
    RUNNING = 'RUNNING'
    INFRA_CREATED = 'INFRA_CREATED'
    NOT_STARTED = 'NOT_STARTED'

class BromyExecutionRequest(Base):
    __tablename__ = "bromy_execution_request"

    id = Column(pgUUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_name = Column(String, nullable=False)
    test_name = Column(String, nullable=False)
    status = Column(pgEnum(ExecutionRequestStatus, name="ExecutionRequestStatusEnum"), nullable=False)
    selenium_script = Column(String, nullable=False)

    infra_on_demand = Column(Boolean, nullable=False)
    destroy_infra_after_finish = Column(Boolean, nullable=False, default=True)
    infra_zone = Column(pgArray(String), nullable=True)
    infra_machine = Column(pgArray(String), nullable=True)
    infra_bot_count = Column(pgArray(Integer), nullable=True)

    script_exec_count = Column(Integer, nullable=False)
    side_runner_mode = Column(Boolean, nullable=False, default=False)
    script_send_keys_param = Column(String, nullable=True)

    infra_creation_time = Column(Integer, nullable=True)
    infra_deletion_time = Column(Integer, nullable=True)
    script_execution_start = Column(BigInteger, nullable=True)
    script_execution_end = Column(BigInteger, nullable=True)
    
    executions = relationship("BromyExecution", back_populates="execution_request")


class ExecutionStatus(str, Enum):
    SUCCESS = 'SUCCESS'
    FAILED = 'FAILED'
    RUNNING = 'RUNNING'
    NOT_STARTED = 'NOT_STARTED'

class BromyExecution(Base):
    __tablename__ = "bromy_execution"

    id = Column(pgUUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    status = Column(pgEnum(ExecutionStatus, name="ExecutionStatusEnum"), nullable=False)
    result = Column(String, nullable=False)
    log = Column(String, nullable=True)
    execution_time = Column(Integer, nullable=True)
    send_keys_param = Column(String, nullable=True)

    execution_request_id = Column(pgUUID, ForeignKey("bromy_execution_request.id", name='fk_exc_to_req'))
    execution_request = relationship("BromyExecutionRequest", back_populates="executions")
