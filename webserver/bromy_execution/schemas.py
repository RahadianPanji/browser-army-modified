from pydantic import BaseModel
from bromy_execution import models
import uuid, json

class ExecutionSchema(BaseModel):
    executionId: uuid.UUID
    status: models.ExecutionStatus
    result: str
    log: str | None
    executionTime: int | None
    sendKeysParams: dict[str, str]

    def from_models(bromy_execution: models.BromyExecution):
        return ExecutionSchema(
            executionId = bromy_execution.id,
            status = bromy_execution.status,
            result = bromy_execution.result,
            log = bromy_execution.log,
            executionTime = bromy_execution.execution_time,
            sendKeysParams = json.loads(bromy_execution.send_keys_param)
        )


class NodeConfig(BaseModel):
    zone: str
    machineType: str
    botCount: int

class InfraParam(BaseModel):
    useOnDemandInfra: bool
    destroyInfraAfterFinish: bool = True
    nodesConfig: list[NodeConfig]

class ScriptParam(BaseModel):
    execCount: int
    sideRunnerMode: bool = False
    sendKeysParams: dict[str, list[str]]

class ProjectMetadata(BaseModel):
    projectName: str
    testName: str

class ElapsedTime(BaseModel):
    infraCreationTime: int | None
    infraDeletionTime: int | None
    scriptExecutionTime: int | None

class ExecutionRequestSchema(BaseModel):
    requestId: uuid.UUID
    executions: list[ExecutionSchema]
    status: models.ExecutionRequestStatus
    seleniumScript: str
    infraParams: InfraParam
    scriptParams: ScriptParam
    elapsedTime: ElapsedTime
    projectMetadata: ProjectMetadata

    def from_models(bromy_exec_req: models.BromyExecutionRequest):
        return ExecutionRequestSchema(
            requestId = bromy_exec_req.id,
            executions = [ExecutionSchema.from_models(db_exec) for db_exec in bromy_exec_req.executions],
            status = bromy_exec_req.status,
            seleniumScript = bromy_exec_req.selenium_script,
            infraParams = InfraParam(
                            useOnDemandInfra = bromy_exec_req.infra_on_demand,
                            destroyInfraAfterFinish = bromy_exec_req.destroy_infra_after_finish,
                            nodesConfig = [
                                NodeConfig(
                                    zone = bromy_exec_req.infra_zone[i],
                                    machineType = bromy_exec_req.infra_machine[i],
                                    botCount = bromy_exec_req.infra_bot_count[i],
                                ) for i in range(len(bromy_exec_req.infra_zone))
                            ] 
                        ),
            scriptParams = ScriptParam(
                            execCount = bromy_exec_req.script_exec_count,
                            sideRunnerMode = bromy_exec_req.side_runner_mode,
                            sendKeysParams = json.loads(bromy_exec_req.script_send_keys_param),
                        ),
            elapsedTime = ElapsedTime(
                            infraCreationTime = bromy_exec_req.infra_creation_time,
                            infraDeletionTime = bromy_exec_req.infra_deletion_time,
                            scriptExecutionTime = bromy_exec_req.script_execution_end - bromy_exec_req.script_execution_start 
                                                  if bromy_exec_req.script_execution_end else None,
                        ),
            projectMetadata = ProjectMetadata(
                                projectName = bromy_exec_req.project_name,
                                testName = bromy_exec_req.test_name,
                            ),
        )

    
    
class CreateExecutionRequest(BaseModel):
    seleniumScript: str
    infraParams: InfraParam
    scriptParams: ScriptParam
    projectMetadata: ProjectMetadata

class DeleteExecutionRequest(BaseModel):
    id: str
