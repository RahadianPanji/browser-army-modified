from fastapi import FastAPI
from contextlib import asynccontextmanager

from bromy_execution import service
from bromy_master import postman as master_postman

@asynccontextmanager
async def lifespan(app: FastAPI):
    # Before Startup
    master_postman.resume_connection()
    service.ensure_persistent_infra()
    yield
    # After Shutdown
